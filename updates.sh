#!/usr/bin/env bash

get_current_version() {
	grep -P "^$1\t" current | cut -f 2
}

get_aur_version() {
	if [[ ${1} == *-git ]]; then
		rm -rf /tmp/${1}
		git clone -q https://aur.archlinux.org/${1}.git /tmp/${1}
		pushd /tmp/${1} >/dev/null
		makepkg -d --nobuild >/dev/null
		makepkg --printsrcinfo | grep pkgver | cut -d'=' -f2 | xargs
		popd >/dev/null
	else
		curl -s "https://aur.archlinux.org/rpc/?v=5&type=info&arg=${1}" | jq -r .results[0].Version
	fi
}

first=1
add_to_update_stack() {
  if [[ ${first} -eq 1 ]]; then
    mv build.template.yml build.yml
    first=0
  fi

  pkg=$1
  echo "build:$pkg:
  extends: .build-template
  variables:
    PKG_NAME: $pkg" >> build.yml
}

for pkg in $(grep -P "\t" current | cut -f 1); do
  if [[ "$FORCED" == *"$pkg"* ]]; then
    echo "---> FORCED, ADDED TO UPDATE STACK"
    add_to_update_stack "$pkg"
    continue
  fi

	current_ver=$(get_current_version "$pkg")
	current_ver=${current_ver/1:/}
	aur_ver=$(get_aur_version "$pkg")
	aur_ver=${aur_ver/1:/}
	ver_diff=$(vercmp "${current_ver}" "${aur_ver}")

  echo "$pkg"
	echo "-> AUR Version:  '$aur_ver'"
	echo "-> Repo Version: '$current_ver'"
	if [[ ("$current_ver") && (${ver_diff} -eq 0) ]]; then
		echo "---> UP TO DATE"
	elif [[ ("$current_ver") && (${ver_diff} -gt 0) ]]; then
		echo "---> EXISTING IS NEWER"
	else
		echo "---> OUTDATED, ADDED TO UPDATE STACK"
		add_to_update_stack "$pkg"
	fi
done

for pkg in $(cat buildlist.txt); do
  if ! grep -wq "$pkg" current; then
    echo "$pkg"
    echo "---> NEW, ADDED TO UPDATE STACK"
    add_to_update_stack "$pkg"
  fi
done
