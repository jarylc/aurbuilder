<?php
$password = '(password)';
if (isset($password))
{
    if (empty($_REQUEST['p']) || $_REQUEST['p'] !== $password) {
        header("HTTP/1.1 401 Unauthorized.");
        die();
    }
}
$input = file_get_contents("php://input");
$json  = json_decode($input);
if (!is_object($json) || empty($json->object_attributes)) {
    header("HTTP/1.1 401 Unauthorized.");
    die();
}
if (empty($json->object_attributes->status) || $json->object_attributes->status != "success") {
    header("HTTP/1.1 204 Ignored.");
    die();
}
shell_exec('(cd /var/www/pkg; git reset --hard origin/master; git lfs prune) >/dev/null 2>/var/www/error.log &');
header("HTTP/1.1 200 Ok.");
?>
