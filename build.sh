#!/bin/bash

WORKDIR="/tmp/ci"
SRCDIR="$PWD"

TITLE_FMT="\e[32m\e[1m"
NO_FMT="\e[0m"

CURRENT_PACKAGE=""
CURRENT_DEPENDENCY_TREE=""

set -e
trap 'echo "Building $CURRENT_PACKAGE, dependency tree: $CURRENT_DEPENDENCY_TREE"' EXIT

invoke_pacman() {
	sudo pacman "$@"
}

download_package() {
	curl --location "https://aur.archlinux.org/cgit/aur.git/snapshot/$1.tar.gz" >"$1.tar.gz"
}

extract_package() {
	bsdtar --no-xattrs -xf "$1.tar.gz"
}

package_get_makedepends() {
	grep 'makedepends =' .SRCINFO | sed 's/\tmakedepends = //'
}

package_get_checkdepends() {
	grep 'checkdepends =' .SRCINFO | sed 's/\tcheckdepends = //'
}

package_get_depends() {
	grep -P '\s+depends =' .SRCINFO | sed 's/\tdepends = //'
}

install_package() {
	invoke_pacman -U --noconfirm "$WORKDIR/$1/"*.pkg.tar.xz
}

save_package() {
	cp "$WORKDIR/$1/"*.pkg.tar.xz "$SRCDIR/build"
}

install_dep_if_needed() {
	# Search for package in current repositories
	# If package is not available we need to build it first
	local dep="$1"
	local target=$(pacman -T "$dep")

	if [[ "$target" ]]; then
		echo -n "Dependency $dep is not satisfied"

		local old_deps_tree="$CURRENT_DEPENDENCY_TREE"
		CURRENT_DEPENDENCY_TREE="$CURRENT_DEPENDENCY_TREE <- $dep"

		local match=$(pacman -Sl | grep " ${target/[<>=]*/} ")
		if [[ -z "$match" ]]; then
			echo ", install it from AUR..."
			build_package "${target/[<>=]*/}"
			install_package "${target/[<>=]*/}"
			#save_package "${target/[<>=]*/}"
		else
			echo ", install it from repositories..."
			invoke_pacman -S --noconfirm "$target"
		fi

		CURRENT_DEPENDENCY_TREE="$old_deps_tree"
	else
		echo "Dependency $dep is satisfied."
	fi
}

recipe_deps() {
	"$SRCDIR/recipes/apply.sh" "$1" deps
}

recipe_pre() {
	"$SRCDIR/recipes/apply.sh" "$1" pre
}

recipe_post() {
	"$SRCDIR/recipes/apply.sh" "$1" post
}

build_package() {
	local pkg="$1"

	pushd "$WORKDIR" >/dev/null

	echo -e "${TITLE_FMT}====== Build package $pkg ======${NO_FMT}"

	echo -e "${TITLE_FMT}==> Download package from AUR${NO_FMT}"
	download_package "$pkg"

	echo -e "${TITLE_FMT}==> Extract package${NO_FMT}"
	extract_package "$pkg"

	pushd "$pkg" >/dev/null

	export SRCDIR
	export PKGBUILD=$(pwd)/PKGBUILD
	export SRCINFO=$(pwd)/.SRCINFO
	export REPOSITORY_NAME
	export REPOSITORY_URL

	recipe_deps "$pkg"
	recipe_pre "$pkg"

	local old_deps_tree="$CURRENT_DEPENDENCY_TREE"
	CURRENT_DEPENDENCY_TREE="$CURRENT_DEPENDENCY_TREE (make)"
	echo -e "${TITLE_FMT}==> Install make dependencies...${NO_FMT}"
	for dep in $(package_get_makedepends); do
		install_dep_if_needed "${dep}"
	done

	CURRENT_DEPENDENCY_TREE="$old_deps_tree (check)"
	echo -e "${TITLE_FMT}==> Install check dependencies...${NO_FMT}"
	for dep in $(package_get_checkdepends); do
		install_dep_if_needed "${dep}"
	done

	CURRENT_DEPENDENCY_TREE="$old_deps_tree (runtime)"
	echo -e "${TITLE_FMT}==> Install runtime dependencies...${NO_FMT}"
	for dep in $(package_get_depends); do
		install_dep_if_needed "${dep}"
	done

	CURRENT_DEPENDENCY_TREE="$old_deps_tree"

	echo -e "${TITLE_FMT}==> Call makepkg...${NO_FMT}"
	makepkg -s --noconfirm

	recipe_post "$pkg"

	popd >/dev/null
	popd >/dev/null
}

create_dirs() {
	if [[ ! -d "$WORKDIR" ]]; then
		mkdir -p "$WORKDIR"
	fi

	if [[ ! -d "$SRCDIR/build" ]]; then
		mkdir -p "$SRCDIR/build"
	fi
}

force_rebuild() {
	echo "To force rebuild pass 'yes' as fourth parameter"
	if [[ "${FORCE_REBUILD}" == "yes" ]]; then
		echo "Force update"
		create_dirs
		build_package "$CURRENT_PACKAGE"
		save_package "$CURRENT_PACKAGE"
	else
		echo "No forced update"
	fi
}

if [[ -z "$1" ]]; then
	echo "Error: package name not specified as first parameter."
	exit 1
fi

if [[ -z "$2" ]]; then
	echo "Error: repository name not defined as second parameter."
	exit 2
else
	REPOSITORY_NAME="$2"
fi

if [[ -z "$3" ]]; then
	echo "Error: repository url not defined as third parameter."
	exit 3
else
	REPOSITORY_URL="$3"
fi

CURRENT_PACKAGE="$1"
CURRENT_DEPENDENCY_TREE="$1"

create_dirs
build_package "$1"
save_package "$1"

trap - EXIT
