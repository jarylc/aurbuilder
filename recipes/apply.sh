#!/bin/bash

DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null && pwd)"

if [[ -f "$DIR/$1.sh" ]]; then
	source "$DIR/$1.sh"

	TITLE_FMT="\e[32m\e[1m"
	NO_FMT="\e[0m"

	case "$2" in
	"deps")
		if [[ ${#DEPENDENCIES[@]} -gt 0 ]]; then
			echo -e "${TITLE_FMT}==> Recipe $1: Installing dependencies: ${DEPENDENCIES[@]}${NO_FMT}"
			sudo pacman -S --noconfirm "${DEPENDENCIES[@]}"
		fi

		if [[ ${#AUR_DEPENDENCIES[@]} -gt 0 ]]; then
			echo -e "${TITLE_FMT}==> Recipe $1: Installing aur dependencies from repository ${REPOSITORY_NAME}: ${DEPENDENCIES[@]}${NO_FMT}"
			filename=$(grep "${AUR_DEPENDENCIES[@]}" "$SRCDIR/current" | cut -f 3)
			curl "${REPOSITORY_URL}/${filename}" >/tmp/pkg
			sudo pacman -U --noconfirm /tmp/pkg
			rm /tmp/pkg
		fi
		;;

	"pre")
		if [[ $(type -t 'pre_build') == "function" ]]; then
			echo -e "${TITLE_FMT}==> Recipe $1: Execute pre-build script${NO_FMT}"
			pre_build
		fi
		;;

	"post")
		if [[ $(type -t 'post_build') == "function" ]]; then
			echo -e "${TITLE_FMT}==> Recipe $1: Execute post-build script${NO_FMT}"
			post_build
		fi
		;;
	esac
fi
