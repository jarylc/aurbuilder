#!/bin/bash

DEPENDENCIES=(rustup)

pre_build() {
	rustup toolchain install stable
}
