#!/bin/bash

get_database() {
	local REPOSITORY_NAME="$1"
	local REPOSITORY_URL="$2"

	mkdir current-repo
	curl -so ${REPOSITORY_NAME}.db.tar.gz "${REPOSITORY_URL}/${REPOSITORY_NAME}.db.tar.gz"
	bsdtar --no-xattrs -xz -C current-repo -f ${REPOSITORY_NAME}.db.tar.gz
}

generate() {
	echo "" > current
	for i in current-repo/*; do
		if [[ -d "$i" ]]; then
			pkgname=$(grep -A 1 '%NAME%' "$i/desc" | tail -n 1)
			pkgver=$(grep -A 1 '%VERSION%' "$i/desc" | tail -n 1)
			pkgfilename=$(grep -A 1 '%FILENAME%' "$i/desc" | tail -n 1)
			echo -e "$pkgname\t$pkgver\t$pkgfilename" >>current
		fi
	done
}

usage() {
	echo "$0 <repository name> <repository url>"
}

if [[ $# -ne 2 ]]; then
	usage
	exit 1
fi

get_database "$1" "$2"
generate
