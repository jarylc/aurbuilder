#!/bin/bash

usage() {
	echo "$0 <repository name>"
}

if [[ $# -ne 1 ]]; then
	usage
	exit 1
fi

pushd packages >/dev/null

git lfs install && git lfs pull
git lfs track "*.xz"
git lfs track "*.gz"

mkdir -p public
mv "../build/"* public || exit 0
repo-add -R -n "public/${REPO_NAME}.db.tar.gz" "public/"*".pkg.tar.xz" || true
rm -f "public/"*".old"

pushd public >/dev/null

# generate commit message
git add -A
COMMIT_MSG="Automated CI update\nUpdated:\n"
for file in $(git diff --staged --name-only .); do
	if ! [[ ${file} =~ \.((db)|(files)) ]]; then
		pkg=$(basename ${file})
		pkg=${pkg/.pkg.tar.xz/}
		COMMIT_MSG+="$pkg "
		echo "$pkg updated"
	fi
done

# delete any packages in blacklist
COMMIT_MSG+="\n\nRemoved:\n"
while IFS= read -r pkg || [[ -n "$pkg" ]]; do
  repo-remove ${1}.db.tar.gz ${pkg} && COMMIT_MSG+="$pkg " && echo "Removed: $pkg" || true;
  rm -f ${pkg}-*
done <../../blacklist.txt

# delete old files
rm -f *.old

# generate files list
rm -f index.html
touch index.html
for file in *; do
	if [[ ${file} != index.html ]]; then
		echo "<a href='./${file}'>${file}</a><br>" >> index.html;
	fi
done
echo -e "User-agent: *\nDisallow: /" > robots.txt

popd >/dev/null

# commit
git config user.email "git@jarylchng.com"
git config user.name "Jaryl Chng (CI)"
git add -A
git commit -m"$(echo -e ${COMMIT_MSG})"

# keep only 2 commit histories
git config alias.rebase-last-two '!b="$(git branch --no-color | cut -c3-)" ; h="$(git rev-parse $b)" ; echo "Current branch: $b $h" ; c="$(git rev-parse $b~1)" ; echo "Recreating $b branch with initial commit $c..." ; git checkout --orphan new-start $c ; git commit -C $c ; git rebase --onto new-start $c $b ; git branch -d new-start ; git gc'
git rebase-last-two

git lfs prune --force --verify-remote --verify-unreachable
git push -f origin master

popd >/dev/null
