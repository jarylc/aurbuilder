# Automation: Archlinux AUR Builder
## About
This project leverages on **Gitlab CI/CD** to build specified applications in the [.gitlab.ci.yml](https://gitlab.com/jarylc/aurbuilder/blob/master/.gitlab-ci.yml) file.

After building, the packages are uploaded to another git repository [aurbuilder-pkg](https://gitlab.com/jarylc/aurbuilder-pkg).

In that repository, we will host it using gitlab-pages at this link [https://jarylc.gitlab.io/aurbuilder-pkg/](https://jarylc.gitlab.io/aurbuilder-pkg/)

Optionally, package names declared in [blacklist.txt](https://gitlab.com/jarylc/aurbuilder/blob/master/blacklist.txt) will be removed from the repository on the next run if it exists.

Finally, also an optional step, a [webhook](https://gitlab.com/jarylc/aurbuilder/blob/master/webhook.php) is sent to my personal server to mirror the repository when the pages build pipeline is successful.

## Basic forking instructions
1. Change the build list in `buildlist.txt` accordingly
2. Set these variables on the Gitlab CI/CD project settings page
    - REPO_NAME - name of output repository
    - REPO_URL - URL where the output repository resides
    - REPO_GIT - SSH URL of output Git repository
    - SSH_KEY - (PROTECTED & MASKED) base64 encoded SSH private key (you can run: `echo $(cat ~/.ssh/id_rsa) | base64 -w 0`)
    - SSH_KNOWN_HOSTS - known hosts entry of the Git repository (Gitlab: `gitlab.com,35.231.145.151 ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBFSMqzJeV9rUzU4kWitGjeR4PWSa29SPqJ1fVkhtj3Hw9xjLVXVYrU9QlYWrOLXBpQ6KWjbjTDTdDkoohFzgbEY=`)
3. Optionally set a schedule to run the automation (I've set it to 6pm +8).

## Add my existing repository to Pacman
If you wish to use my repository as your packages you need may already be built, add this to the bottom of your `/etc/pacman.conf`:
```conf
[jarylchng]
SigLevel = Optional TrustAll
Server = https://jarylc.gitlab.io/aurbuilder-pkg/
```
Since the packages are unsigned, you will have to include `SigLevel = Optional TrustAll` to ignore that fact.
